# !usr/bin/python3

"""

sort.py

This script is used to list the files in the Downloads folder and then sort them into the correct folders according to their extensions

Author: BigoudOps bigoudops.fr

"""
import glob
import os
import pathlib
import shutil
import subprocess
from os import path
from gi.repository import GLib

# calling XDG Directories for getting right name of user folders

Folder_User = GLib.get_home_dir()
Folder_Document = GLib.get_user_special_dir(
    GLib.UserDirectory.DIRECTORY_DOCUMENTS)
Folder_Download = GLib.get_user_special_dir(
    GLib.UserDirectory.DIRECTORY_DOWNLOAD)
Folder_Pictures = GLib.get_user_special_dir(
    GLib.UserDirectory.DIRECTORY_PICTURES)
Folder_Templates = GLib.get_user_special_dir(
    GLib.UserDirectory.DIRECTORY_TEMPLATES)
Folder_Music = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_MUSIC)
Folder_Public_Share = GLib.get_user_special_dir(
    GLib.UserDirectory.DIRECTORY_PUBLIC_SHARE)
Folder_Videos = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_VIDEOS)

ExtensionDocuments = ['.txt', '.pdf', '.odt', '.docx', '.wps']
ExtensionPictures = ['.jpg', '.png', '.gif', '.jpeg', '.xcf', '.svg', ]
ExtensionMusic = ['.aac', '.flac', '.ogg', '.mp3', '.wav', '.m3u', '.opus']
ExtensionVideo = ['.avi', '.3gp', '.mkv', '.mov', '.mp4', '.webm']
ExtensionSoftware = ['.deb', '.tar', '.gz', '.zip', '.xz']
ExtensionISO = ['.iso', '.img', '.vmdk', '.qcow2']
ExtensionBDD = ['.kdb', '.kdbx']
ExtensionAndroid = ['.apk']
ExtensionPsVita = ['.vpk']
ExtensionVSCodium = ['.vsix']
ExtensionDL = ['.torrent']

print("Your folder names are ")
print(Folder_User)
print(f"{Folder_Download}           {Folder_Pictures}")
print(f"{Folder_Document}           {Folder_Music}")
print(f"{Folder_Public_Share}           {Folder_Videos}")
print()
print()
print("Now i'm gonna list files in the folder " + Folder_Download)
print()
print()
print(os.listdir(Folder_Download))

softwareFiles = os.path.join(Folder_Document, 'ProgramFiles')
iso = os.path.join(Folder_Document, 'ISO')
documentsFiles = os.path.join(Folder_Document, 'PDF')
pictureFiles = os.path.join(Folder_Pictures,)
musicFiles = os.path.join(Folder_Music,)
videoFiles = os.path.join(Folder_Videos,)
bddFiles = os.path.join(Folder_User, 'BDD')
vpkFiles = os.path.join(Folder_User, 'PSVITA')
androidFiles = os.path.join(Folder_User, 'apkAndroid')
vsixFiles = os.path.join(Folder_User, 'VsixCODIUM')
torrentFiles = os.path.join(Folder_User, 'TORRENT')
files = glob.glob(os.path.join(Folder_Download, '*'))

def moveFileIncrement(file, folder):
    if os.path.exists(file):
        fileName, fileExtension = os.path.splitext(file)
        fileName = fileName.split('-')
        fileName = fileName[0]
        fileName = f'{fileName}-{len(os.listdir(folder))}'
        os.rename(file, fileName + fileExtension)
        shutil.move(fileName + fileExtension, folder)
    else:
        shutil.move(file, folder)

    print(f"{file} moved to {folder}")
for file in files:
    if path.splitext(file)[1] in ExtensionDocuments:
        if not path.exists(documentsFiles):
            os.makedirs(documentsFiles)
        moveFileIncrement(file, documentsFiles)
        if not path.exists(softwareFiles):
            os.makedirs(softwareFiles)
    elif path.splitext(file)[1] in ExtensionPictures:
        if not path.exists(pictureFiles):
            os.makedirs(pictureFiles)
        moveFileIncrement(file, pictureFiles)
    elif path.splitext(file)[1] in ExtensionMusic:
        if not path.exists(musicFiles):
            os.makedirs(musicFiles)
        moveFileIncrement(file, musicFiles)
    elif path.splitext(file)[1] in ExtensionVideo:
        if not path.exists(videoFiles):
            os.makedirs(videoFiles)
        moveFileIncrement(file, videoFiles)
    elif path.splitext(file)[1] in iso:
        if not path.exists(iso):
            os.makedirs(iso)
        moveFileIncrement(file, iso)
    elif path.splitext(file)[1] in ExtensionBDD:
        if not path.exists(bddFiles):
            os.makedirs(bddFiles)
        moveFileIncrement(file, bddFiles)
    elif path.splitext(file)[1] in ExtensionAndroid:
        if not path.exists(androidFiles):
            os.makedirs(androidFiles)
        moveFileIncrement(file, androidFiles)
    elif path.splitext(file)[1] in ExtensionDL:
        if not path.exists(torrentFiles):
            os.makedirs(torrentFiles)
        moveFileIncrement(file, torrentFiles)
    elif path.splitext(file)[1] in ExtensionVSCodium:
        if not path.splitext(file)[1] in vsixFiles:
            if not path.exists(vsixFiles):
                os.makedirs(vsixFiles)
            moveFileIncrement(file, vsixFiles)
    elif path.splitext(file)[1] in ExtensionPsVita:
        if not path.exists(vpkFiles):
            os.makedirs(vpkFiles)
        moveFileIncrement(file, vpkFiles)
    else:
        print(f"{file} is not a file I can move")
print("you still have these files to sort manually:")
print()
print(os.listdir(Folder_Download))
